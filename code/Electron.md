# Electron

公式は[こちら](https://www.electronjs.org/). ぶっちゃけ, ハッハー英語でもいけるぜメルツェェェェェル！！！な人は記事探しよりもひとまずここをさらっと目を通して単語を抑えていくのがいいのではないでしょうか.

Q. Electronとは何ぞや?

A. JS, HTML, CSSでネイティブアプリが作れる便利なフレームワークだそうです. 似たようなものとしては[クロスプラットフォームフレームワーク比較 2020（Flutter, React Native, Xamarin, Unity）](https://qiita.com/nskydiving/items/c13c949cc17c6f980a67)こちらをご覧ください. 

Electronは気に入らないという方がもし現れましたら, 自分に十分な力こそパワーもしくは[ボディーソープのCMの人](https://www.nicovideo.jp/watch/sm17444108)なみのパワーが備わっていることを確認の上, ガンガン書き換えていって下さい.(例えば, webpackでwatchしているとは言えどいちいち閉じてからelectron.しないといけなくて面倒だからflutterいじってみたいなど. この場合はXbeeAPIみたいなZigBee規格への便利変換ライブラリの整備を多分自分で行うはめになります.)

Q. メインプロセスとレンダラープロセスとは?

A. [ここ](https://www.electronjs.org/docs/tutorial/application-architecture)読むの

Q. あーだこーだよりも動くものちょーだい

A. [ここ](https://ics.media/entry/7298/)とか, 公式のQuick Startとかをやりましょう.

Q. なんか色々聞くけどセキュリティ大丈夫なの?

A. (うちの企画では)多分だめぽ. というわけで[ここ](https://www.electronjs.org/docs/tutorial/security)読んでコードを手直しして頂けると嬉しいです. 完全に外部から切り離されているならnode integrationがtrueになってても大丈夫なんでしょうけど, Ghostは雲予想とかでネットにつないでいますのでので.
