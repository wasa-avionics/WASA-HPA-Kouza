# 0から始めるJavascript(とHTML, CSS)とTypescript

地上用表示計Ghostにおいては, AltJSのひとつのTypescriptを使用しています. TypescriptはJavascriptを内包するように注意深く開発されている言語です. Typescriptはその名前の通り, Javascriptに型をつけたものです. 

<details><summary>雑談</summary>

型があるとなんで嬉しいのかについては, ここで説明するよりも, JavascriptとTypescriptそれぞれに触れてみて, 実際に開発して体験してみた方が早いと思いますのでここではあーだこーだとは言わないです. 
<s>(それでも, Object.Keysで返ってくるのがstring[]でこちらで定義したなんらかのtypeではないなど不便なところもありますが.(これは意図的なもので, 型システムが壊れるのを防止するためとのことです.　[こちら](https://github.com/microsoft/TypeScript/pull/12253#issuecomment-263132208)を読むの. 余談ですが, ここで質問に答えてらっしゃるAnders Hejlsberg氏はC#とTypescriptの開発者でいわゆるレジェンド的な方です.))</s>
</details>

---
早速外部の記事に丸投げであれなのですが, 以下の記事以上のものを現状, 自分は書くことができませんのであしからず. 

Javascript(ついでにHTMLとは何ぞやという話も)/Typescriptの学習にはuhyoさんの記事が大変参考になると思います. 

<details><summary>雑談2</summary>

<s>最近, twitterで語尾に「ぇ」をつけるのをやめたそうです. 書いている記事の質と中級者の記事の言い訳とJavaScriptの歴史で書かれている, 記事を書いたモチベと合わせて, 爪を隠すというか, 怖い人が多い中で個人的には親近感が湧くというかで好きだったのですけどね.</s>
</details>

ここでわざわざ載せなくても検索の上位にくるので大丈夫とは思いますが, 一応リンクを貼っておきます.  

Javascriptについては[JavaScript初級者から中級者になろう](https://uhyohyo.net/javascript/)をご覧ください. Typescriptについては, [TypeScriptの型入門](https://uhyohyo.net/javascript/)と, リンク先下部のあわせて読みたいをご覧ください. 他にも, [Type-level TypeScript](https://ryota-ka.hatenablog.com/entry/2017/12/21/000000)という面白そうなブログがありました.


また, JavascriptのPromiseに焦点を当てたものとして, [JavaScript Promiseの本](https://azu.github.io/promises-book/)もリンクを貼っておきますので是非, Ghostの新規開発者の方は上の記事と合わせて読んでみてください.

基本的なことは大体上に挙げた記事の中にのっていますが, もし, 細かい仕様などが気になった時には, [mozillaのJavascriptについてのガイド](https://developer.mozilla.org/ja/docs/Web/JavaScript)を見ると早いと思われます. 

その他, とても微妙で場当たり的な問題にあたってしまった場合には, 誰かに聞く前に一度, 同じ問題にあたっている人が, 解説記事に乗せそうなキーワードをなんとか上手く抜き出して検索かけてみましょう.(多分個人ブログやQiitaの記事がヒットします.) 英語にできるなら英語でも検索をかけてみましょう.(主にgithubのissueとかstack over flowとかの検索用.)

さて, 上の記事群を読んでいき, 気になったことを検索かけていく中で, おそらく, commonjsがどうのだの, ECMAScriptではどうのだと出てくると思います. というわけで[JavaScriptが辿った変遷](https://qiita.com/naoki_mochizuki/items/cc6ef57d35ba6a69117f)を読みましょう.

# 懺悔

typescriptをトランスパイルは, @typesのないモジュールを使用しているので, .tsファイル内でもrequireを使いたいことと, ついでにelectronのメインとレンダラーの二つにバンドルしてしまいたいのでwebpackのts-loaderを用いて行っていますが, 実は, cssとか, その他の画像ファイル(サイズの小さな画像しかなかったので、、こちらはなんどBase64にしちゃってまして, npm scriptにcp書けばいいじゃんといわれればそうなのですが, JQueryのattrでstring内で相対パス指定している箇所があって, webpackでバンドルする以上, 階層関係が崩れるので一緒にバンドルするしかなかった.)とかもまとめてwebpackでバンドルしてしまって, 出力のHTML内に埋め込まれるようにしています. webpackの設定が闇になっていくので避けたかったのですが, 致し方なくという感じですごめんなさい. <s>ReactにしてNextがどんなもんか試したい...試したくない?</s>

一応, 設定ファイルを記述する上で[公式ドキュメント](https://webpack.js.org/concepts/)以外に主に参考にしたものを,<s>記憶の限り</s>Ghostの開発引継ぎ用に下記に残しておきます.

[TypeScriptをプロダクト開発に使う上でのベストプラクティスと心得](https://qiita.com/jagaapple/items/ce0da04be28c35dc7d4f)

[url-loader](https://github.com/webpack-contrib/url-loader)

[img-loader, file-loader, url-loader, resolve-url-loader の違い](https://e-joint.jp/644/)

[webpackとBabelの基本を理解する(1) ―webpack編―](https://qiita.com/koedamon/items/3e64612d22f3473f36a4)

[file-loaderで画像を扱うときのパス指定](https://qiita.com/tomi_linka/items/ef66a60950939618c449)

[Incompatibility with node-serialport (bindings.node initialization failed) #3696](https://github.com/electron-userland/electron-builder/issues/3696)

[Webpackのexternalsについて](https://kigh-memo.hatenablog.com/entry/2018/09/22/220836)

[webpack can not pack sqlite3 #698](https://github.com/mapbox/node-sqlite3/issues/698)

[webpackで開発用/本番用の設定を分ける](https://qiita.com/teinen_qiita/items/4e828ac30221efb624e1)

[tsconfig.jsonの全オプションを理解する（随時追加中）](https://qiita.com/ryokkkke/items/390647a7c26933940470)

[最新版で学ぶwebpack 4入門 JavaScriptのモジュールバンドラ](https://ics.media/entry/12140/)

[Babel 7でTypeScriptをトランスパイルしつつ型チェックをする 〜webpack 4 + Babel 7 + TypeScript + TypeScript EsLint + Prettierの開発環境を構築する〜](https://qiita.com/soarflat/items/d583356e46250a529ed5)

[がんばらない webpack](https://wakame.hatenablog.jp/entry/2019/01/05/134128)

[Webpack — The Confusing Parts](https://medium.com/@rajaraodv/webpack-the-confusing-parts-58712f8fcad9#.lx82mu7eb)

[How to set up a Webpack project [Tutorial]](https://www.robinwieruch.de/webpack-setup-tutorial/)

